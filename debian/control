Source: libperlx-define-perl
Section: perl
Priority: optional
Build-Depends:
 libkeyword-simple-perl,
 libnamespace-clean-perl,
 debhelper-compat (= 13),
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders:
 Jonas Smedegaard <dr@jones.dk>,
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libperlx-define-perl.git
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libperlx-define-perl
Homepage: https://metacpan.org/release/PerlX-Define
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-perl

Package: libperlx-define-perl
Architecture: all
Depends:
 libkeyword-simple-perl,
 libnamespace-clean-perl,
 ${misc:Depends},
 ${perl:Depends},
Description: cute syntax for defining constants
 PerlX::Define is a yet another module for defining constants.
 .
 Differences from constant.pm:
 .
  * Cute syntax.
    Like constant.pm, constants get defined at compile time,
    not run time.
  * Requires Perl 5.12 or above.
    If you're lucky enough to be able to free yourself
    from the shackles of supporting decade-old versions of Perl,
    PerlX::Define is your friend.
  * Only supports scalar constants.
    List constants are rarely useful.
    Your constant can of course be a reference to an array or hash,
    but this module doesn't attempt
    to make the referred-to structure read only.
  * Doesn't try to handle some of the things constant.pm does
    like declaring constants using fully-qualified names,
    or defining constants pointing at magic scalars.
 .
 Prior to version 0.100, PerlX::Define was bundled with Moops.
